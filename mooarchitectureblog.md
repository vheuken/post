A Walkthrough of an Early Clojure Codebase
=====

_Or: Documenting my own stupidity so you can be smarter_

Lately, I’ve been thinking about the things that have brought me to my current level of competency in programming. Being self-taught, I never had much rhyme or reason in developing my curriculum. I just followed whatever my interests were at the time. Those interests would point me to books, blog posts, open source projects to contribute to, and even attempts at more substantial, nontrivial projects of my own.

While I could not have done it without the foundation laid through reading and smaller-scale projects, attempts at non-trivial projects have taught me substantially more about software development than anything else. More specifically, taking a retrospective look at my architectural, logical, technological, and design decisions. It’s very humbling to take a magnifying glass to your mistakes, ask why you made them, and figure out how to avoid them in the future.

This article will be going over my current project, Moo Player. Starting from a high level overview of the architecture, it will proceed to a low level look at the flow of some of the functionality. There will be code throughout. Some of the code will be clean. A lot of it will be rather crufty. There’s certainly a level of embarrassment showing off code one isn’t proud of. At the end of the day, though, it’s just code. A bit of temporary embarrassment is a small price to pay to give back to a community that has helped me grow so much as a developer.


The Project
-----

_That thing you’re going to be reading about_

| ![](moo0.png) |
|:--:|
| _Excuse the [programmer art][programmer-art]..._ |


Moo Player is a collaborative media player. What that means is that users can create media players that they and their friends can control from their individual devices, as if they’re in a physical space together sharing one device. Everything from the order of the tracks in the playlist to the position of the playing media is synchronized across all connected clients. Each player is isolated to a room with a chat alongside it.

[TODO: video of Moo Player in action]

If you’re wondering about the name, it’s a pun on “Mumu Player”. Mumu Player is a now defunct webapp that shared the same core functionality, albeit with fewer features.

| ![](mumu.jpg) |
|:--:|
| _Don't ask me to explain the squids._ |


The Stack
-----

_The stuff I used to build it_

* Backend
  * [Clojure](https://clojure.org/)
  * [Immutant](http://immutant.org/)
    * Undertow (web server)
    * Infinispan (caching)
  * [Sente](https://github.com/ptaoussanis/sente) (Websockets)
  * [Monger](https://github.com/michaelklishin/monger) (MongoDB)
* Frontend
  * [Clojurescript](https://clojurescript.org/)
  * [Reagent](https://reagent-project.github.io/) (React)
  * [re-frame](https://github.com/Day8/re-frame) (Event handling)  


The Architecture
-----

_A bird's eye view_

At the highest level of abstraction, the project can be separated into two sections: the server and the client. Unlike how web applications are traditionally written, this project aims to keep the client and server entirely separate. In the current iteration, a disconnected client can do everything a connected client can do except adding new media and sending chat messages.  These cases will be handled in the future.

This article will focus on the server. There’s certainly things to be said about the client, but I want to keep a focused scope. For a vague idea of how the client architecture looks, [check out the re-frame README][re-frame]. It’s an excellent read.

Going down a few levels of abstraction, the server is roughly separated into three sections: **Input**, **Events**, and **Actions**.

**Input** refers to the section of the codebase that handles incoming data from clients. At the moment, this is currently just an HTTP/websockets frontend. The data received from clients is used alongside server-provided data to create events to feed the event handler. The architectural goal to strive for is to be able to be able to support various input mechanisms and data formats. For now, though, it follows the [YAGNI][yagni] principle.

**Events** are where all the logic goes. These are completely [pure functions][pure-functions] that generate actions from event data. They return a sequential structure of actions to be performed. The significance of this design decision is that it allows for [simple, easy][simple-made-easy], and performant testing.

**Actions** are the things that change the world. Those things include database modifications, network/file IO, and cache modifications. Actions are, by nature, impure functions. They should be a sequence of steps with no actual logic. No conditionals, no loops. Those all go inside events. This significantly lessens the need for tests. Testing IO directly is difficult, tedious, and fragile. Anything that can be done to lessen it is a big win for maintainability. Having the logic outside of actions also means that changing implementation details can be done without having to work around the logic.

Basically:
1. **Input** takes data from clients and creates **Events**.
2. **Events** process said data and create **Actions** for the server to perform
3. **Actions**, finally, make the server actually do things.

This provides a simple, one way flow: **Input → Events → Actions**. All of the logic is isolated in the middle, all of the stuff that is inherently impure stays at the edges.

I want to be clear: while new code is written with these principles in mind, these were not in place from the project's conception. There’s still some logic in some actions and some impure event handlers. Some of those spots will come up later.

Walkthrough
-----

_Wading through a swamp of parenthesis_

| ![](lisp_cycles.png) |
|:--:|
| _Clojure is a Lisp, if you didn't know. Credit to [xkcd][xkcd-lisp]._ |


Now it's time to dive into some code. Stuff like HTTP routing will be ignored so the focus can stay on the core architecture.

[TODO: explain more. maybe explain which components will be talked about]

#### Initial Client Connection

[TODO: show picture of initial client state]

#### User Join

[TODO: show picture of a name in user field]

[TODO: show picture of user in the users list. maybe with a chat message]

#### Player seek

[TODO: webm of seek]

#### Remove a track

[TODO: talk about how we're skipping over some things]


Closing Remarks
-----

_[Nothing Left to Say](https://www.youtube.com/watch?v=8gSRsOdPbII)_

[xkcd-lisp]: https://xkcd.com/297/

[programmer-art]: https://en.wikipedia.org/wiki/Programmer_art

[re-frame]: https://github.com/Day8/re-frame/blob/master/README.md

[pure-functions]: https://en.wikipedia.org/wiki/Pure_function

[simple-made-easy]: https://www.infoq.com/presentations/Simple-Made-Easy

[yagni]: https://en.wikipedia.org/wiki/You_aren't_gonna_need_it
